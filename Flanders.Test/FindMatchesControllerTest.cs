using System.Linq;
using Xunit;
using Flanders.Controllers;
using Flanders.Services;

namespace Flanders.Test.Controllers
{
    public class FindMatchesControllerTest
    {
        private readonly FindMatchesService _service;

        public FindMatchesControllerTest()
        {
            _service = new FindMatchesService();
        }

        [Fact]
        public void EmptyTest()
        {
            var result = _service.Search("", "");
            Assert.Empty(result);
        }
        [Fact]
        public void SingleTest()
        {
            var result = _service.Search("testtest", "tt");
            Assert.Single(result);
        }
        [Fact]
        public void MultipleTest()
        {
            var result = _service.Search("testtest", "es");
            Assert.Equal(2, result.Count());
        }
        [Fact]
        public void CaseSensitiveTest()
        {
            var result = _service.Search("TesTtESt", "es");
            Assert.Equal(2, result.Count()); 
        }
        [Fact]
        public void IndexLowerBoundaryTest()
        {
            var result = _service.Search("testtest", "t");
            Assert.Equal(1, result.ToArray()[0].Index);
        }
        [Fact]
        public void IndexUpperBoundaryTest()
        {
            var result = _service.Search("testtest", "t");
            Assert.Equal(8, result.ToArray()[3].Index);
        }
        [Fact]
        public void BiggerSubtextTest()
        {
            var result = _service.Search("tt", "test");
            Assert.Empty(result);
        }
    }
}
