import { Injectable, Inject } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()

export class FindMatchesService {
  public matches: FindMatches[];
  constructor(private http: HttpClient) { }

  runSearch(text: string, subtext: string): Observable<FindMatches[]> {
    let params = new HttpParams();
    params = params.append('text', text);
    params = params.append('subtext', subtext);

    return this.http.get<FindMatches[]>(environment.BASE_URL + 'api/FindMatches/Search', { params: params });
  }
}

interface FindMatches
{
  index: number;
}
