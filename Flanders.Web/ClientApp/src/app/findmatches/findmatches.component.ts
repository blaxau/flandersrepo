import { Component } from '@angular/core';
import { FindMatchesService } from './findmatches.service';

@Component({
  selector: 'findmatches',
  templateUrl: './findmatches.component.html'
})
export class FindMatchesComponent {
  private text: string;
  private subtext: string;

  public matches: FindMatches[];

  constructor(private findMatchesService: FindMatchesService) { }

  runSearch() {
    this.findMatchesService.runSearch(this.text, this.subtext).subscribe(results => this.matches = results);
  }

  onTextChange(txtText: string) {
    this.text = txtText;
    this.runSearch();
  }

  onSubtextChange(txtSubtext: string) {
    this.subtext = txtSubtext;
    this.runSearch();
  }
}

interface FindMatches
{
  index: number;
}
