import { async, TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { FindMatchesService } from './findmatches.service';

describe('FindMatchesService', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [FindMatchesService]
    })
  }));

  it('should not break on null',
    inject(
      [HttpTestingController, FindMatchesService],
      (httpMock: HttpTestingController, searchService: FindMatchesService) => {
        searchService.runSearch(null, null).subscribe(r => expect(r.length == 0))
      })
  );

  it('should return 1 match',
    inject(
      [HttpTestingController, FindMatchesService],
      (httpMock: HttpTestingController, searchService: FindMatchesService) => {
        searchService.runSearch("TeST", "e").subscribe(r => expect(r.length == 1))
      })
  );


  it('should return 2 matches',
    inject(
      [HttpTestingController, FindMatchesService],
      (httpMock: HttpTestingController, searchService: FindMatchesService) => {
        searchService.runSearch("testtesttest", "tt").subscribe(r => expect(r.length == 2))
      })
  );

  it('pass lower boundary',
    inject(
      [HttpTestingController, FindMatchesService],
      (httpMock: HttpTestingController, searchService: FindMatchesService) => {
        searchService.runSearch('test', 't').subscribe(r => expect(r[0].index == 1))
      })
  );

  it('pass upper boundary',
    inject(
      [HttpTestingController, FindMatchesService],
      (httpMock: HttpTestingController, searchService: FindMatchesService) => {
        searchService.runSearch('test', 't').subscribe(r => expect(r[1].index == 4))
      })
  );

  it('pass mixed case',
    inject(
      [HttpTestingController, FindMatchesService],
      (httpMock: HttpTestingController, searchService: FindMatchesService) => {
        searchService.runSearch('TEST', 't').subscribe(r => expect(r.length == 2))
      })
  );

  it('pass ascii characters',
    inject(
      [HttpTestingController, FindMatchesService],
      (httpMock: HttpTestingController, searchService: FindMatchesService) => {
        searchService.runSearch('TEÄT', 'Ä').subscribe(r => expect(r[0].index == 3))
      })
  );

});
