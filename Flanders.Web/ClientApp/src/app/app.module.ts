import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { FindMatchesComponent } from './findmatches/findmatches.component';

import { FindMatchesService } from './findmatches/findmatches.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    FindMatchesComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: FindMatchesComponent, pathMatch: 'full' }
    ])
  ],
  providers: [FindMatchesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
