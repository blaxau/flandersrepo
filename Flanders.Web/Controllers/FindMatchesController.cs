using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flanders.Models.Pocos;
using Flanders.Services;
using Microsoft.AspNetCore.Mvc;

namespace Flanders.Controllers
{
    [Route("api/[controller]")]
    public class FindMatchesController : Controller
    {
        [HttpGet("[action]")]
        public IEnumerable<FindMatches> Search([FromServices]IFindMatchesService service, string text, string subtext)
        {
            return service.Search(text, subtext);
        }

        
    }
}
