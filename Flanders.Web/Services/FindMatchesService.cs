﻿using Flanders.Models.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flanders.Services
{
   

    public interface IFindMatchesService
    {
        IEnumerable<FindMatches> Search(string text, string subtext);
    }
    public class FindMatchesService : IFindMatchesService
    {
        public IEnumerable<FindMatches> Search(string text, string subtext)
        {
            List<FindMatches> results = new List<FindMatches>();

            if (!string.IsNullOrEmpty(text) && !string.IsNullOrEmpty(subtext))
            {
                int currentPos = 0;
                int nextMatchIndex = 0;

                while (currentPos >= 0)
                {
                    try
                    {
                        nextMatchIndex = text.IndexOf(subtext, currentPos, StringComparison.InvariantCultureIgnoreCase);
                        if (nextMatchIndex > -1)
                        {
                            currentPos = ++nextMatchIndex;
                            results.Add(new FindMatches { Index = currentPos });
                        }
                        else
                            break;
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            return results.ToArray();
        }
    }
}
